import arcade


def On_Draw(delta_time):
    arcade.start_render()
    BallYX = ball_move(delta_time)
    On_Draw.PaddleOneY = Paddle_Move(BallYX, On_Draw.PaddleOneY, delta_time, 1)
    On_Draw.PaddleTwoY = Paddle_Move(BallYX, On_Draw.PaddleTwoY, delta_time, 2)
    draw_paddle(On_Draw.PaddleOneX, On_Draw.PaddleOneY)
    draw_paddle(On_Draw.PaddleTwoX, On_Draw.PaddleTwoY)
    point_list = (250, 337), (250, 307), \
                 (250, 277), (250, 247), \
                 (250, 217), (250, 187), \
                 (250, 157), (250, 127), \
                 (250, 97), (250, 67),\
                 (250, 37), (250, 7)
    arcade.draw_lines(point_list, arcade.color.WHITE, 5)


# Static Paddle Variables
On_Draw.PaddleOneX = 20
On_Draw.PaddleOneY = 175

On_Draw.PaddleTwoX = 480
On_Draw.PaddleTwoY = 175


def draw_paddle(x, y):
    arcade.draw_rectangle_filled(x, y, 12, 55, arcade.color.WHITE)


def Paddle_Move(BallYX, y, delta_time, The_Chooser):
    if The_Chooser == 1 and BallYX[0] <= 333:
        if y < BallYX[1] and y < 320:
            y += 100 * delta_time
        elif y > BallYX[1] and y > 35:
            y -= 100 * delta_time
    if The_Chooser == 2 and BallYX[0] >= 166:
        if y < BallYX[1] and y < 320:
            y += 100 * delta_time
        elif y > BallYX[1] and y > 35:
            y -= 100 * delta_time
    return y


def ball_move(delta_time):
    if ball_move.y >= 350 - 7:
        ball_move.BALL_SPEED_Y *= -1
    if ball_move.y <= 0 + 7:
        ball_move.BALL_SPEED_Y *= -1
    ball_move.y += ball_move.BALL_SPEED_Y * delta_time

    if ball_move.x >= 480 - 13:
        ball_move.BALL_SPEED_X *= -1
    if ball_move.x <= 20 + 13:
        ball_move.BALL_SPEED_X *= -1
    ball_move.x += ball_move.BALL_SPEED_X * delta_time
    draw_ball(ball_move.x, ball_move.y)
    return ball_move.x, ball_move.y


ball_move.x = 250
ball_move.y = 175
ball_move.BALL_SPEED_Y = 100
ball_move.BALL_SPEED_X = 100


def draw_ball(x, y):
    arcade.draw_point(x, y, arcade.color.WHITE, 22)


arcade.open_window(500, 350, "P1NG")
arcade.set_background_color(arcade.color.BLACK)


arcade.schedule(On_Draw, 1/30)
arcade.run()
