import arcade


def main():
    arcade.open_window(350, 400,  "Ninbendo GAME MAN")
    arcade.set_background_color(arcade.color.BABY_BLUE)
    arcade.start_render()
    gameman(99, 70)

    arcade.finish_render()
    arcade.run()


def gameman(x, y):
    # DRAW BODY FILLED
    # Main Face
    point_list = (75 + x, 200 + y), (150 + x, 175 + y), \
                 (150 + x, 70 + y), (129 + x, 67 + y), \
                 (129 + x, 50 + y), (75 + x, 67 + y)
    arcade.draw_polygon_filled(point_list, arcade.color.COOL_GREY)
    arcade.draw_arc_filled(129 + x, 70 + y, 40, 40, arcade.color.COOL_GREY, 270, 360)

    # Side
    point_list = (150 + x, 175 + y), (165 + x, 190 + y), (165 + x, 77 + y), (145 + x, 57 + y)
    arcade.draw_polygon_filled(point_list, arcade.color.COOL_GREY)

    # Top Top
    point_list = (75 + x, 200 + y), (150 + x, 175 + y), (165 + x, 190 + y), (90 + x, 215 + y)
    arcade.draw_polygon_filled(point_list, arcade.color.COOL_GREY)

    # DRAW BODY OUTLINES

    # Main Face
    point_list = (75 + x, 200 + y), (75 + x, 67 + y), (127 + x, 50 + y), (75 + x, 67 + y)
    arcade.draw_lines(point_list, arcade.color.COOL_BLACK, 2)
    arcade.draw_arc_outline(127 + x, 70 + y, 40, 40, arcade.color.COOL_BLACK, 270, 360, 4)

    # Side
    point_list = (150 + x, 175 + y), (165 + x, 190 + y), \
                 (165 + x, 77 + y), (145 + x, 57 + y), \
                 (148 + x, 65 + y), (150 + x, 70 + y)
    arcade.draw_polygon_outline(point_list, arcade.color.COOL_BLACK, 2)

    # Top Top
    point_list = (75 + x, 200 + y), (150 + x, 175 + y), \
                 (165 + x, 190 + y), (90 + x, 215 + y)
    arcade.draw_polygon_outline(point_list, arcade.color.COOL_BLACK, 2)

    # DRAW DECALS OUTLINE

    # Mold Lines
    point_list = ((75 + x, 194 + y), (150 + x, 169 + y),   # The front face horizontal mold line.
                  (150 + x, 169 + y), (158 + x, 177 + y),   # The side line horizontal short mold line.
                  (144 + x, 178 + y), (152 + x, 186 + y),   # The top right vertical short mold line.
                  (81 + x, 198 + y), (89 + x, 206 + y),   # The top left vertical short mold line.
                  (144 + x, 178 + y), (144 + x, 172 + y),   # The front face right vertical short mold line.
                  (81 + x, 198 + y), (81 + x, 192 + y),   # The front face left vertical short mold line.
                  (83 + x, 208 + y), (158 + x, 183 + y),   # The top line horizontal long mold line.
                  (158 + x, 183 + y), (158 + x, 69 + y))   # The side line vertical long mold line.
    arcade.draw_lines(point_list, arcade.color.PAYNE_GREY, 2)

    # Off,  On Button
    point_list =(92 + x, 203 + y), (88 + x, 198 + y), \
                (88 + x, 198 + y), (99 + x, 195 + y),  \
                (99 + x, 200 + y),
    arcade.draw_lines(point_list, arcade.color.BLACK_OLIVE, 2)

    # Dark Rim + Screen
    GAMEMAN_DARK_SOULS = (73, 83, 85)
    point_list = (81 + x, 182 + y),  (144 + x, 161 + y),  \
                 (144 + x, 161 + y),  (144 + x, 121 + y), \
                 (81 + x, 182 + y),  (81 + x, 132 + y), \
                 (81 + x, 132 + y), (135 + x, 114 + y)
    arcade.draw_lines(point_list, arcade.color.COOL_BLACK, 2)
    arcade.draw_arc_outline(134 + x, 122 + y, 19, 17, arcade.color.COOL_BLACK, 270, 360, 3)

    point_list = (94 + x, 169 + y),  (130 + x, 157 + y),\
                 (130 + x, 124 + y),  (94 + x, 136 + y)
    arcade.draw_polygon_outline(point_list, arcade.color.LIME_GREEN, 2)

    point_list = (81 + x, 182 + y), (144 + x, 161 + y), \
                 (144 + x, 121 + y), (135 + x, 114 + y), \
                 (81 + x, 132 + y)
    arcade.draw_polygon_filled(point_list, GAMEMAN_DARK_SOULS)
    arcade.draw_arc_filled(134 + x, 122 + y, 10, 10, GAMEMAN_DARK_SOULS, 270, 360)

    point_list = (94 + x, 169 + y),  (130 + x, 157 + y), \
                 (130 + x, 124 + y),  (94 + x, 136 + y)
    arcade.draw_polygon_filled(point_list, arcade.color.LIME_GREEN)

    # Those colored Lines at the Top of Dark rim
    point_list = (84 + x, 178 + y), (141 + x, 159 + y)
    arcade.draw_lines(point_list, arcade.color.DARK_PINK, 1)

    point_list = (84 + x, 174 + y), (141 + x, 154 + y)
    arcade.draw_lines(point_list, arcade.color.OCEAN_BOAT_BLUE, 1)

    point_list = (100 + x, 170 + y), (133 + x, 159 + y),
    arcade.draw_lines(point_list, GAMEMAN_DARK_SOULS, 6)

    # Battery Light
    arcade.draw_circle_filled(87 + x, 154 + y, 1, arcade.color.CANDY_APPLE_RED)

    # Button A, B + Rim to buttons
    arcade.draw_ellipse_outline(136 + x, 88 + y, 23, 10, arcade.color.PAYNE_GREY, 1, 30)

    arcade.draw_circle_filled(141 + x, 90 + y, 4, arcade.color.RUBY_RED)
    arcade.draw_circle_filled(130 + x, 85 + y, 4, arcade.color.RUBY_RED)

    arcade.draw_arc_outline(141 + x, 90 + y, 3, 3, arcade.color.WHITE, 200, 340, 1, 90)
    arcade.draw_arc_outline(130 + x, 85 + y, 3, 3, arcade.color.WHITE, 200, 340, 1, 90)

    # Button Select,  Start
    arcade.draw_ellipse_filled(112 + x, 78 + y, 13, 4, arcade.color.PAYNE_GREY, 20)
    arcade.draw_ellipse_filled(102 + x, 81 + y, 13, 4, arcade.color.PAYNE_GREY, 20)

    # Directional Pad
    point_list = ((94 + x, 103 + y), (100 + x, 101 + y),
                  (100 + x, 95 + y), (102 + x, 97 + y),
                  (102 + x, 103 + y), (96 + x, 105 + y),
                  (96 + x, 111 + y), (90 + x, 113 + y),
                  (88 + x, 111 + y), (90 + x, 107 + y),
                  (84 + x, 109 + y), (82 + x, 107 + y),
                  (94 + x, 91 + y), (96 + x, 93 + y))
    arcade.draw_polygon_filled(point_list, arcade.color.SMOKY_BLACK)

    point_list = ((94 + x, 103 + y), (100 + x, 101 + y),
                  (100 + x, 95 + y), (94 + x, 97 + y),
                  (94 + x, 91 + y), (88 + x, 93 + y),
                  (88 + x, 99 + y), (82 + x, 101 + y),
                  (82 + x, 107 + y), (88 + x, 105 + y),
                  (88 + x, 111 + y), (94 + x, 109 + y))
    arcade.draw_polygon_filled(point_list, arcade.color.SMOKY_BLACK)
    arcade.draw_polygon_outline(point_list, arcade.color.PAYNE_GREY, 1)

    point_list = ((94 + x, 103 + y), (96 + x, 105 + y),
                  (100 + x, 101 + y), (102 + x, 103 + y),
                  (100 + x, 95 + y), (102 + x, 97 + y),
                  (94 + x, 91 + y), (96 + x, 93 + y),
                  (82 + x, 107 + y), (84 + x, 109 + y),
                  (88 + x, 111 + y), (90 + x, 113 + y),
                  (94 + x, 109 + y), (96 + x, 111 + y))
    arcade.draw_lines(point_list, arcade.color.PAYNE_GREY, 1)

    # Speakers
    arcade.draw_line(124 + x, 70 + y, 132 + x, 60 + y, arcade.color.PAYNE_GREY, 2)
    arcade.draw_line(110 + x, 70 + y, 122 + x, 56 + y, arcade.color.PAYNE_GREY, 2)

    # Volume Control Dial
    arcade.draw_arc_filled(154 + x, 134 + y, 8, 4, arcade.color.BLACK, 0, 180, 270)
    arcade.draw_arc_filled(156 + x, 134 + y, 8, 4, arcade.color.BLACK, 0, 180, 270)

    arcade.draw_arc_outline(154 + x, 134 + y, 8, 4, arcade.color.PAYNE_GREY, 0, 180, 1, 270)
    arcade.draw_arc_outline(156 + x, 134 + y, 8, 4, arcade.color.PAYNE_GREY, 0, 180, 1, 270)

    # That Rectangle I Saw in a Reference Picture That Was On The Top AKA Top of Game Cartridge

    # TEXT
   # start_y = 150
    # start_x = 300
    # arcade.draw_point(start_x,  start_y,  arcade.color.BABY_BLUE, 5)
    # arcade.draw_text("AGHHHHHHHHHHHHH", start_x, start_y, arcade.color.BLACK, 10, width=200,
               #  align="center", anchor_x="center", anchor_y="center", rotation=20)

main ()

