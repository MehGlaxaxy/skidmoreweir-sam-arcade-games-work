# Add two numbers and return the results
"""
def sum_two_numbers(a, b):
    result = a + b
    print (result)
    return result


result = sum_two_numbers(50, 19)
print (result)

sum_two_numbers(4, 14)


# Function that prints the result
def sum_print(a, b):
    result = a + b
    print(result)


# Function that returns the results
def sum_return(a, b):
    result = a + b
    return result


# This prints the sum of 4+4
sum_print(4, 4)

# This does not
sum_return(4, 4)

# This will not set x1 to the sum
# It actually gets a value of 'None'
x1 = sum_print(4, 4)
print("x1 =", x1)

# This will set x2 to the sum
# and print it properly
x2 = sum_return(4, 4)
print("x2 =", x2)
"""
# Create the x variable and set to 44
# Define a simple function that prints x
def f():
    x = 44
    x += 1
    print(x)


# Call the function
f()

# Define a simple function that prints x
def f(x):
    x += 1
    print(x)


# Set y
y = 10
# Call the function
f(y)
# Print y to see if it changed
print(y)
